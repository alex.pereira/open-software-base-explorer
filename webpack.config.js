module.exports = {
  entry: {
    jsonInspector: './static/src/js/json-inspector.jsx'
  },
  output: {
    filename: '[name].js'
  },
  module: {
    loaders: [
      {
        test: /json-inspector.jsx$/,
        loader: 'expose?jsonInspector!babel-loader?presets[]=react,presets[]=es2015'
      },
      {test: /\.css$/, loader: 'style-loader!css-loader'}
    ]
  }
};
